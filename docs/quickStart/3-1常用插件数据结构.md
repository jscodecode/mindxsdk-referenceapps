# 常用插件数据结构
在stream中数据通过挂载metadata在各个插件之间传递，本章节提供内部protobuf数据的图示。
- metadata的详细数据定义请参考《用户指南》中的“C Metadata proto 文件”章节
- MindX SDK使用Protocol Buffer 3格式
- protoSample文件夹中的样例提供了如何组装一个MxpiFrame结构并使用Senddata传入stream。
[点击跳转](https://gitee.com/ascend/mindxsdk-referenceapps/tree/master/tutorials/protocolSample)
- 实际组装时根据需要可以直接组装非顶级的protobuf结构，例如使用SendProtobuf()时直接组装出MxpiVisionList作为protobuf数据

## 样例代码
在protocolSample样例中:
- main.cpp使用SendData()和SendProtobuf()构建了metadata的protobuf结构传入stream并输出序列化信息
- protobufMake.cpp内包含构建和读取protobuf结构的几个示例，注意本文件不可直接用于编译
- main.py使用SendProtobuf()构建了metadata的protobuf结构传入stream并输出序列化信息

## Protocol协议相关信息
Protocol Buffer (简称Protobuf) 是开源的性能优异、跨语言、跨平台的序列化库。  
如需相关参考文档，请访问:https://developers.google.com/protocol-buffers/docs/proto3  
如需查找开发对应的API，请访问:https://developers.google.com/protocol-buffers/docs/reference/overview

## MxpiDumpData
![image.png](img/1622260210336.png 'image.png')

## MxpiDataType

### MxpiMemoryType
>内存类型，枚举值
```
enum MxpiMemoryType {
    MXPI_MEMORY_HOST = 0;
    MXPI_MEMORY_DEVICE = 1;
    MXPI_MEMORY_DVPP = 2;
}
```
### MxpiFrame
>用于存放视频和图像帧，包含帧信息，数据信息。  
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622260262069.png 'image.png')
- MxpiFrameInfo                // 帧信息
- MxpiVisionList               // 视频、图像数据列表
- MxpiVision                         // 视频、图像数据结构
- MxpiMetaHeader               // 数据结构头，存放插件信息的vector,用于序列化
>***parentName          //此变量在后续版本中将会被弃用，请使用“dataSource”***
>memberId           //MxpiVisionList中对应存放的MxpiVision索引
>dataSource          //依赖数据的索引名,通过该索引获取依赖的元数据
- MxpiVisionInfo                // 视频、图像描述信息
- MxpiVisionData              // 视频、图像数据内容
>dataPtr // 视频、图像内存指针
freeFunc       // 视频、图像内存销毁函数
dataStr            // bytes数据类型 序列化成json时会自动进行base64编码

### MxpiObjectList
>用于存放目标相关的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622260504942.png 'image.png')
- MxpiObjectList               // 目标列表
- MxpiObject                  // 目标数据结构
> repeated MxpiClass :本结构中此处“MxpiMetaHeader”无效
- MxpiClass                   // 类别信息数据结构

### MxpiClassList
>用于存放类别相关的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622260608712.png 'image.png')
- MxpiClassList              // 类别信息列表
- MxpiClass                   // 类别信息数据结构

### MxpiAttributeList
>用于存放属性信息相关的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622260762270.png 'image.png')
- MxpiAttributeList         // 属性信息列表
- MxpiAttribute            // 属性信息数据结构

### MxpiTrackLetList
>用于存放目标匹配相关的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622260859278.png 'image.png')
- MxpiTrackLetList            //目标匹配列表
- MxpiTrackLet                 //目标匹配数据结构
>uint32 age              // 目标“存活”帧数
uint32 hits              // 目标被成功匹配帧数
int32 trackFlag           // 状态
    
### MxpiTensorPackageList
>用于存放模型tensor组合列表的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622261006414.png 'image.png')
- MxpiTensorPackageList			// 模型tensor组合列表
- MxpiTensorPackage				// 模型tensor组合数据结构
- MxpiTensor				    // 模型tensor数据结构
>uint64 tensorDataPtr             // tensor内存指针
uint64 freeFunc            // tensor内存销毁函数
int32 tensorShape       // tensor维度

### MxpiFeatureVectorList
>用于存放特征向量列表的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622261081407.png 'image.png')
- MxpiFeatureVectorList			// 特征向量列表
- MxpiFeatureVector				// 特征向量数据结构

### MxpiKeyPointAndAngleList
>用于存放关键点和角度信息列表的数据
浅色背景对应repeated字段,代表该结构可重复
![image.png](img/1622261209163.png 'image.png')
- MxpiKeyPointAndAngleList          // 关键点和角度信息列表
- MxpiKeyPointAndAngle     // 关键点和角度数据结构
>float keyPointsVec      // 人脸对应的五个关键点信息
    float angleYaw                 // 偏航角
    float anglePitch                // 俯仰角
    float angleRoll                    // 横滚角